import unicorn
from collections import defaultdict

class UTypes:
    def __init__(self, arch, mode):
        self.uc = unicorn.Uc(arch, mode)
        self.mapped_pages = set()
        self.symbols = defaultdict(set)
        self.code_hooks = defaultdict(set)
        self.uc.hook_add(unicorn.UC_HOOK_CODE, self._the_code_hook)
        self.scratchpad = None
        self.stack_ptr = None
        self.lr = None
        self.trace = False
        self.strings = {}
        self._call = None
        self._regs = []
        self._consts = None
        self._reg_prefix = None
        if arch == unicorn.UC_ARCH_X86:
            if mode == unicorn.UC_MODE_32:
                self._call = self._call_x86
                self._regs = ['EAX', 'ECX', 'EDX', 'EBX', 'ESP', 'EBP', 'ESI', 'EDI']
                self._consts = unicorn.x86_const
                self._reg_prefix = 'UC_X86_REG_'
            elif mode == unicorn.UC_MODE_64:
                self._call = self._call_x86_64
                self._regs = ['RAX', 'RCX', 'RDX', 'RBX', 'RSP', 'RBP', 'RSI', 'RDI']+['R'+str(i) for i in range(8, 16)]
                self._consts = unicorn.x86_const
                self._reg_prefix = 'UC_X86_REG_'
        elif arch == unicorn.UC_ARCH_ARM64:
            if mode == unicorn.UC_MODE_LITTLE_ENDIAN:
                self._call = self._call_arm64
                self._regs = ['PC', 'SP']+['X'+str(i) for i in range(31)]
                self._consts = unicorn.arm64_const
                self._reg_prefix = 'UC_ARM64_REG_'
    def create_stack(self, base, size):
        assert base % 4096 == 0 and size % 4096 == 0
        self.scratchpad = base
        self.stack_ptr = base + size
        self.map_memory_range(base, base + size)
    def map_memory_range(self, start, end):
        start -= start % 4096
        end += (-end) % 4096
        i = start
        while i < end:
            while i < end and i in self.mapped_pages:
                i += 4096
            if i == end: break
            j = i
            while j < end and j not in self.mapped_pages:
                self.mapped_pages.add(j)
                j += 4096
            self.uc.mem_map(i, j-i)
            i = j
    def load_elf(self, blob, *, erase_bss=False):
        assert blob[:6] == b'\x7fELF\x02\x01' # 64-bit little endian
        # parse program headers and load segments into memory
        entry = int.from_bytes(blob[24:32], 'little')
        phoff = int.from_bytes(blob[32:40], 'little')
        phnum = int.from_bytes(blob[56:58], 'little')
        for i in range(phnum):
            chk = blob[phoff+56*i:phoff+56*i+56]
            if chk[:4] != b'\1\0\0\0':
                continue
            offset = int.from_bytes(chk[8:16], 'little')
            vaddr = int.from_bytes(chk[16:24], 'little')
            filesz = int.from_bytes(chk[32:40], 'little')
            memsz = int.from_bytes(chk[40:48], 'little')
            data = blob[offset:offset+filesz] + bytes(memsz-filesz)
            self.map_memory_range(vaddr, vaddr+memsz) # rwx
            self.uc.mem_write(vaddr, data)
        # parse section headers
        shoff = int.from_bytes(blob[40:48], 'little')
        shnum = int.from_bytes(blob[60:62], 'little')
        sections = []
        for i in range(shnum):
            chk = blob[shoff+64*i:shoff+64*i+64]
            tp = int.from_bytes(chk[4:8], 'little')
            addr = int.from_bytes(chk[16:24], 'little')
            offset = int.from_bytes(chk[24:32], 'little')
            size = int.from_bytes(chk[32:40], 'little')
            link = int.from_bytes(chk[40:44], 'little')
            if tp == 8 and erase_bss:
                self.uc.mem_write(addr, bytes(size))
            sections.append((tp, blob[offset:offset+size], link))
        # parse symtab/strtab pairs and register symbols
        for tp, symtab, link in sections:
            if tp != 2: continue
            assert sections[link][0] == 3
            strtab = sections[link][1]
            for i in range(0, len(symtab), 24):
                nameidx = int.from_bytes(symtab[i:i+4], 'little')
                name = strtab[nameidx:strtab.find(b'\0', nameidx)].decode('ascii')
                addr = int.from_bytes(symtab[i+8:i+16], 'little')
                self.symbols[name].add(addr)
                self.symbols[addr].add(name)
    def dlsym(self, addr):
        return self.symbols[addr]
    def dladdr(self, addr):
        assert isinstance(addr, int)
        base, sym = max((i, j) for i, s in self.symbols.items() if isinstance(i, int) and i <= addr for j in s)
        return (sym, addr-base)
    def hook_add(self, addr_or_symbol, fn):
        self.code_hooks[addr_or_symbol].add(fn)
    def hook_del(self, addr_or_symbol, fn=None):
        if fn is None:
            del self.code_hooks[addr_or_symbol]
        else:
            self.code_hooks[addr_or_symbol].discard(fn)
    def _the_code_hook(self, q, addr, *args):
        if self.trace:
            print('at', hex(addr), self.dlsym(addr))
        for i in self.code_hooks.get(addr, ()):
            i(q, addr, *args)
        for s in self.symbols.get(addr, ()):
            for i in self.code_hooks.get(s, ()):
                i(q, addr, *args)
    def string(self, s):
        if s is None:
            return 0
        if s == 0:
            return None
        if isinstance(s, int):
            ans = b''
            p = s
            while not ans.endswith(b'\0'):
                ans += self.uc.mem_read(p, 1)
                p += 1
            ans = ans[:-1]
            if ans not in self.strings:
                self.strings[ans] = s
            return ans
        if isinstance(s, str):
            s = s.encode('ascii')
        if s in self.strings:
            return self.strings[s]
        ans = self.scratchpad
        self.scratchpad += len(s) + 1
        self.uc.mem_write(ans, s+b'\0')
        return ans
    def _emu_with_error_report(self, start, end):
        try: self.uc.emu_start(start, end)
        except:
            for i in self._regs:
                print(i, '=', hex(self.uc.reg_read(getattr(self._consts, self._reg_prefix+i))))
            raise
    def _call_x86(self, fn, *args):
        data = b''.join(i.to_bytes(4, 'little') for i in args)
        data = self.lr.to_bytes(4, 'little') + data + bytes((-len(data)) % 16)
        self.uc.mem_write(self.stack_ptr-len(data), data)
        self.uc.reg_write(unicorn.x86_const.UC_X86_REG_ESP, self.stack_ptr-len(data))
        self._emu_with_error_report(fn, self.lr)
        return self.uc.reg_read(unicorn.x86_const.UC_X86_REG_EAX)
    def _call_x86_64(self, fn, *args):
        for r, a in zip(('RDI', 'RSI', 'RDX', 'RCX', 'R8', 'R9'), args):
            self.uc.reg_write(getattr(unicorn.x86_const, 'UC_X86_REG_'+r), a)
        data = b''.join(i.to_bytes(8, 'little') for i in args[6:])
        data = self.lr.to_bytes(8, 'little') + data + bytes((-len(data)) % 16)
        self.uc.mem_write(self.stack_ptr-len(data), data)
        self.uc.reg_write(unicorn.x86_const.UC_X86_REG_RSP, self.stack_ptr-len(data))
        self._emu_with_error_report(fn, self.lr)
        return self.uc.reg_read(unicorn.x86_const.UC_X86_REG_RAX)
    def _call_arm64(self, fn, *args):
        for i, a in enumerate(args[:8]):
            self.uc.reg_write(getattr(unicorn.arm64_const, 'UC_ARM64_REG_X'+str(i)), a)
        data = b''.join(i.to_bytes(8, 'little') for i in args[8:])
        data += bytes((-len(data)) % 16)
        self.uc.mem_write(self.stack_ptr-len(data), data)
        self.uc.reg_write(unicorn.arm64_const.UC_ARM64_REG_SP, self.stack_ptr-len(data))
        self.uc.reg_write(unicorn.arm64_const.UC_ARM64_REG_LR, self.lr)
        self._emu_with_error_report(fn, self.lr)
        return self.uc.reg_read(unicorn.arm64_const.UC_ARM64_REG_X0)
    def call(self, fn, *args):
        if isinstance(fn, str):
            fn = next(iter(self.dlsym(fn)))
        return self._call(fn, *args)
