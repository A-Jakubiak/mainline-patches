try: gdb
except NameError:
    import os, sys
    do_json = False
    if '--json' in sys.argv:
        del sys.argv[sys.argv.index('--json')]
        do_json = True
    if len(sys.argv) != 4:
        print('''\
usage: dump_clocks.py <vmlinux> <ect_dump> <output> [--json]

    <vmlinux> is the compiled binary of the vendor Linux kernel.
    <ect_dump> is either the binary dump of the "ect" memory region declared in the devicetree, or a text dump of /sys/kernel/debug/ect/pll_list
    <output> is the output file to save clock data to.

    If --json is specified, the data will be saved in JSON format.
''')
        exit(0)
    vmlinux = sys.argv[1]
    ect = sys.argv[2]
    out_path = sys.argv[3]
    if not vmlinux.startswith('/'):
        vmlinux = './' + vmlinux
    params = '(ect, out_path, do_json, sys.path)'
    os.execlp('gdb', 'gdb', vmlinux, '-ex', 'py import sys; '+params+' = '+repr(eval(params)), '-x', __file__, '-ex', 'quit')

import json, ect_parser

with open(ect, 'rb') as file:
    ect = ect_parser.parse_ect(file.read())

VCLK_DFS = 2
VCLK_DFS_SWITCH = 4

vclks = []

def gdb_str(x):
    ans = b''
    while True:
        q = int(x[len(ans)])
        if not q: return ans.decode('ascii')
        ans += bytes((q,))

for i in gdb.execute('info variables ^exynos[0-9]*_.*_vclks$', to_string=True).split('static struct init_vclk ')[1:]:
    name = i.split('[', 1)[0]
    assert name.startswith('exynos') and name.endswith('_vclks')
    vclkp = gdb.parse_and_eval(name)
    for i in range(vclkp.type.sizeof // vclkp[0].type.sizeof):
        vclk = vclkp[i]
        if vclk['vclk_flags'] & (VCLK_DFS | VCLK_DFS_SWITCH):
            continue
        idx = int(vclk['id'])
        while len(vclks) <= idx:
            vclks.append(None)
        vclks[idx] = gdb.parse_and_eval('&(vclk_'+gdb_str(vclk['name'])+').vclk')

v2psfrmap = gdb.parse_and_eval('v2psfrmap')

clk_busy = set()
clk_done = set()
clk_decl = []
clk_print = []
vclk_print = []

def addr_cache(f):
    cache = {}
    def ans(addr):
        k = int(addr)
        if k not in cache:
            cache[k] = f(addr)
        return cache[k]
    return ans

@addr_cache
def print_clk(addr):
    if not addr:
        return None if do_json else 'NULL'
    try: name = gdb_str(addr['name'])
    except gdb.error:
        name = hex(addr)
    if not name.startswith('clk_'):
        name = 'clk_'+name
    idx = int(addr['id'])
    if idx == 0:
        return None if do_json else 'CLK_NULL'
    clk_busy.add(int(addr))
    parent = addr['parent']
    parent_name = print_clk(parent)
    q = []
    qh = []
    for i, j in (('offset', ''), ('status', 's_'), ('enable', 'e_')):
        offset = int(addr[i])
        shift = int(addr[j+'shift'])
        width = int(addr[j+'width'])
        if offset:
            which, offset = divmod(offset, 2**16)
            reg_addr = offset + int(v2psfrmap[which]['pa'])
            if do_json:
                q.append({'addr': reg_addr, 'bank': which, 'offset': offset, 'shift': shift, 'width': width})
            else:
                q.append('(%s, %d, %d)'%(hex(reg_addr), shift, width))
            qh.append(True)
        else:
            qh.append(False)
    kind, idx = divmod(idx, 2**24)
    if kind == 1: # fixed_rate
        addr = gdb.parse_and_eval('(struct pwrcal_clk_fixed_rate*)'+str(int(addr)))
        rate = int(addr['fixed_rate'])
        gate = addr['gate']
        assert not q
        if do_json:
            clk_print.append({'type': 'fixed_rate', 'name': name, 'parent': parent_name, 'rate': rate, 'gate': print_clk(gate)})
        else:
            clk_print.append('DEFINE_clk_fixed_rate(%s, %s, %s, %s)'%(name, parent_name, rate, print_clk(gate)))
    elif kind == 2: # fixed_factor
        addr = gdb.parse_and_eval('(struct pwrcal_clk_fixed_factor*)'+str(int(addr)))
        ratio = int(addr['ratio'])
        gate = addr['gate']
        assert not q
        if do_json:
            clk_print.append({'type': 'fixed_div', 'name': name, 'parent': parent_name, 'ratio': ratio, 'gate': print_clk(gate)})
        else:
            clk_print.append('DEFINE_clk_fixed_div(%s, %s, %s, %s)'%(name, parent_name, ratio, print_clk(gate)))
    elif kind == 5: # pll
        assert parent == 0 or parent['id'] == 0, (parent, parent_name)
        addr = gdb.parse_and_eval('(struct pwrcal_pll*)'+str(int(addr)))
        ops = addr['ops']
        if '<pll141xx_ops>' in str(ops):
            sfx = '_141xx'
        elif '<pll1431x_ops>' in str(ops):
            sfx = '_1431x'
        else:
            assert False
        assert len(q) == 2
        tp = int(addr['type'])
        try: rates = ect[name[4:]]
        except KeyError: rates = []
        if not do_json:
            rates = ['{{{rate}, {pdiv}, {mdiv}, {sdiv}, {kdiv}}}'.format(**i) for i in rates]
        mux = addr['mux']
        if int(mux) in clk_busy:
            try: mux_name = gdb_str(mux['name'])
            except gdb.error: mux_name = hex(mux)
            if not mux_name.startswith('clk_'):
                mux_name = 'clk_'+mux_name
            if do_json:
                mux = mux_name
            else:
                clk_decl.append('DECLARE_clk_mux(%s)'%mux_name)
                mux = '&'+mux_name
        else:
            mux = print_clk(mux)
        if int(addr) not in clk_done:
            clk_done.add(int(addr))
            if do_json:
                clk_print.append({'type': 'pll'+sfx, 'name': name, 'main': q[0], 'status': q[1], 'version': tp, 'mux': mux, 'rates': rates})
            else:
                clk_print.append('DEFINE_clk_pll%s(%s, %s, %s, %d, %d, %s, {%s})'%(sfx, name, q[0], q[1], tp, len(rates), mux, ', '.join(rates)))
    elif kind == 6: # mux
        assert parent == 0
        addr = gdb.parse_and_eval('(struct pwrcal_mux*)'+str(int(addr)))
        p_parents = addr['parents']
        n_parents = int(addr['num_parents'])
        gate = addr['gate']
        parents = [print_clk(p_parents[i]) for i in range(n_parents)]
        assert qh[0] and qh[2], qh
        if do_json:
            clk_print.append({'type': 'mux', 'name': name, 'parents': parents, 'main': q[0], 'status': q[1] if len(q) == 3 else None, 'enable': q[-1], 'gate': print_clk(gate)})
        else:
            clk_print.append('DEFINE_clk_mux(%s, (%s), %s, %s, %s, %s)'%(name, ', '.join(parents), q[0], q[1] if len(q) == 3 else '(0, 0, 0)', q[-1], print_clk(gate)))
    elif kind == 7: # div
        assert qh == [True, True, False], qh
        addr = gdb.parse_and_eval('(struct pwrcal_div*)'+str(int(addr)))
        gate = addr['gate']
        if do_json:
            clk_print.append({'type': 'div', 'name': name, 'parent': parent_name, 'main': q[0], 'status': q[1], 'gate': print_clk(gate)})
        else:
            clk_print.append('DEFINE_clk_div(%s, %s, %s, %s, %s)'%(name, parent_name, q[0], q[1], print_clk(gate)))
    elif kind == 8: # gate
        assert qh == [True, False, False], qh
        if do_json:
            clk_print.append({'type': 'gate', 'name': name, 'parent': parent_name, 'main': q[0]})
        else:
            clk_print.append('DEFINE_clk_gate(%s, %s, %s)'%(name, parent_name, q[0]))
    else:
        if do_json:
            clk_print.append({'type': 'unknown', 'name': name, 'parent': parent_name, 'regs': q, 'kind': kind, 'index': idx})
        else:
            clk_print.append('DEFINE_clk_unknown(%s, %s, {%s}, %s, %s)'%(name, parent_name, ', '.join(q), hex(kind), hex(idx)))
    clk_busy.discard(addr)
    return name if do_json else '&'+name

@addr_cache
def print_vclk(addr):
    if not addr:
        return None if do_json else 'NULL'
    try: name = gdb_str(addr['name'])
    except gdb.error: name = hex(addr)
    if not name.startswith('vclk_'):
        name = 'vclk_'+name
    parent = addr['parent']
    parent_name = print_vclk(parent)
    vtable = addr['ops']
    if not vtable:
        return None if do_json else 'VCLK_NULL'
    try: vtable_name = str(vtable).split('<', 1)[1].split('>', 1)[0]
    except IndexError: vtable_name = hex(vtable)
    if vtable_name == 'grpgate_ops':
        addr = gdb.parse_and_eval('(struct pwrcal_vclk_grpgate*)'+str(int(addr)))
        gatep = addr['gates']
        gates = []
        while True:
            gate = gatep[0]
            gatep += 1
            if not gate['id']: #'<clk_0>' in str(gate):
                break
            gates.append(print_clk(gate))
        if do_json:
            vclk_print.append({'type': 'grpgate', 'name': name, 'parent': parent_name, 'gates': gates})
        else:
            vclk_print.append('DEFINE_grpgate(%s, %s, (%s))'%(name, parent_name, ', '.join(i for i in gates)))
    elif vtable_name == 'pxmxdx_ops':
        addr = gdb.parse_and_eval('(struct pwrcal_vclk_pxmxdx*)'+str(int(addr)))
        plist = addr['clk_list']
        children = []
        while True:
            clk = plist['clk']
            if not clk['id']: #'<clk_0>' in str(clk):
                break
            config0 = plist['config0']
            config1 = plist['config1']
            plist += 1
            children.append('{%s, %d, %d}'%(print_clk(clk), config0, config1))
        if do_json:
            vclk_print.append({'type': 'pxmxdx', 'name': name, 'parent': parent_name, 'children': children})
        else:
            vclk_print.append('DEFINE_pxmxdx(%s, %s, {%s})'%(name, parent_name, ', '.join(children)))
    elif vtable_name == 'd1_ops':
        addr = gdb.parse_and_eval('(struct pwrcal_vclk_d1*)'+str(int(addr)))
        div = addr['div']
        if do_json:
            vclk_print.append({'type': 'd1', 'name': name, 'parent': parent_name, 'clk': print_clk(div)})
        else:
            vclk_print.append('DEFINE_d1(%s, %s, %s)'%(name, parent_name, print_clk(div)))
    elif vtable_name == 'p1_ops':
        addr = gdb.parse_and_eval('(struct pwrcal_vclk_p1*)'+str(int(addr)))
        pll = addr['pll']
        if do_json:
            vclk_print.append({'type': 'p1', 'name': name, 'parent': parent_name, 'clk': print_clk(pll)})
        else:
            vclk_print.append('DEFINE_p1(%s, %s, %s)'%(name, parent_name, print_clk(pll)))
    elif vtable_name == 'umux_ops':
        addr = gdb.parse_and_eval('(struct pwrcal_vclk_umux*)'+str(int(addr)))
        umux = addr['umux']
        if do_json:
            vclk_print.append({'type': 'umux', 'name': name, 'parent': parent_name, 'clk': print_clk(umux)})
        else:
            vclk_print.append('DEFINE_umux(%s, %s, %s)'%(name, parent_name, print_clk(umux)))
    elif vtable_name == 'm1d1g1_ops':
        addr = gdb.parse_and_eval('(struct pwrcal_vclk_m1d1g1*)'+str(int(addr)))
        mux = addr['mux']
        div = addr['div']
        gate = addr['gate']
        extra_mux = addr['extramux']
        if do_json:
            vclk_print.append({'type': 'm1d1g1', 'name': name, 'parent': parent_name, 'mux': print_clk(mux), 'div': print_clk(div), 'gate': print_clk(gate), 'extmux': print_clk(extra_mux)})
        else:
            vclk_print.append('DEFINE_m1d1g1(%s, %s, %s, %s, %s, %s)'%(name, parent_name, print_clk(mux), print_clk(div), print_clk(gate), print_clk(extra_mux)))
    elif vtable_name == 'dfs_ops':
        if do_json:
            vclk_print.append({'type': 'unimplemented', 'name': name})
        else:
            return 'VCLK_NYI' # NYI
    else:
        if do_json:
            vclk_print.append({'type': 'unknown', 'name': name, 'parent': parent_name, 'vtable': vtable_name})
        else:
            vclk_print.append('DEFINE_unknown(%s, %s, %s)'%(name, parent_name, vtable_name))
    return name if do_json else '&'+name

vclk_names = [print_vclk(i) if i is not None else None for i in vclks]

with open(out_path, 'w') as output:
    if do_json:
        json.dump({
            'clocks': clk_print,
            'virtual_clocks': vclk_print,
            'vclk_table': {k: v for k, v in enumerate(vclk_names) if v is not None},
        }, output, indent=4)
        output.write('\n')
    else:
        print('\n/* CLOCKS */\n', file=output)
        for i in clk_decl:
            print(i, file=output)
        for i in clk_print:
            print(i, file=output)
        print('\n/* VIRTUAL CLOCKS */\n', file=output)
        for i in vclk_print:
            print(i, file=output)
        prev = -1
        print('\nVCLK_TABLE({', file=output)
        for i, j in enumerate(vclk_names):
            if j is not None:
                if i == prev + 1:
                    print('    %s,'%j, file=output)
                else:
                    print('    [%d] = %s,'%(i, j), file=output)
                prev = i
        print('})', file=output)
